package infra.database.migrations;

import infra.database.migrations.interfaces.Migration;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class ActorTableMigration implements Migration {
  private final Connection connection;

  public ActorTableMigration(Connection connection) {
    this.connection = connection;
  }

  @Override
  public void createTable() {
    String create = "CREATE TABLE IF NOT EXISTS actor(" +
      "id INT NOT NULL AUTO_INCREMENT," +
      "name VARCHAR(45) NOT NULL UNIQUE," +
      "bio VARCHAR(255) NOT NULL," +
      "PRIMARY KEY (id));";
    try {
      Statement statement = this.connection.createStatement();
      statement.execute(create);
      System.out.println("Table actor created");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to create table actor " + exception.getMessage());
      throw new RuntimeException("Database Error", exception);
    }
  }

  @Override
  public void truncateTable() {
    String truncate = "TRUNCATE TABLE actor;";
    try {
      Statement statement = this.connection.createStatement();
      statement.execute(truncate);
      System.out.println("Table actor truncated");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to truncate table " + exception.getMessage());
      throw new RuntimeException("Database Error", exception);
    }
  }

  @Override
  public void dropTable() {
    String drop = "DROP TABLE IF EXISTS actor;";
    try {
      Statement statement = this.connection.createStatement();
      statement.execute(drop);
      System.out.println("Table actor dropped");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to drop table actors");
      throw new RuntimeException("Database Error", exception);
    }
  }
}
