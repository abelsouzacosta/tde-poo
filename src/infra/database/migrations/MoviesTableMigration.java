package infra.database.migrations;

import infra.database.migrations.interfaces.Migration;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class MoviesTableMigration implements Migration {
  private final Connection connection;
  public MoviesTableMigration(Connection connection) {
    this.connection = connection;
  }
  @Override
  public void createTable() {
    String create = "CREATE TABLE IF NOT EXISTS movie(" +
      "id INT NOT NULL AUTO_INCREMENT," +
      "title VARCHAR(45) NOT NULL," +
      "director VARCHAR(45) NOT NULL," +
      "release_date DATE NOT NULL," +
      "PRIMARY KEY(ID));";
    try {
      Statement statement = this.connection.createStatement();
      statement.execute(create);
      System.out.println("Table movie created");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to connect to database: " + exception.getMessage());
      throw new RuntimeException("Database error", exception);
    }
  }

  @Override
  public void truncateTable() {
    String truncate = "TRUNCATE TABLE movie;";
    try {
      Statement statement = this.connection.createStatement();
      statement.executeUpdate(truncate);
      System.out.println("Table movie truncated");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to truncate table movie");
      throw new RuntimeException("Database Error", exception);
    }
  }

  @Override
  public void dropTable() {
    String drop = "DROP TABLE IF EXISTS movie;";
    try {
      Statement statement = this.connection.createStatement();
      statement.executeUpdate(drop);
      System.out.println("Table movie dropped");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to drop table movie");
      throw new RuntimeException("Database error", exception);
    }
  }

  public void alterTableAddDescriptionColumn() {
    String alter = "ALTER TABLE movie ADD COLUMN description VARCHAR(255);";
    try {
      Statement statement = this.connection.createStatement();
      statement.executeUpdate(alter);
      System.out.println("Table movie updated");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to alter table movie");
      throw new RuntimeException("Database Error", exception);
    }
  }
}
