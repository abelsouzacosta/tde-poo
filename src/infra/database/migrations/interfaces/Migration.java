package infra.database.migrations.interfaces;

public interface Migration {
  void createTable();
  void dropTable();
  void truncateTable();
}
