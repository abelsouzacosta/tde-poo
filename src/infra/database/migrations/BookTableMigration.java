package infra.database.migrations;

import infra.database.migrations.interfaces.Migration;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class BookTableMigration implements Migration {
  private final Connection connection;

  public BookTableMigration(Connection connection) {
    this.connection = connection;
  }

  @Override
  public void createTable() {
    String create = "CREATE TABLE IF NOT EXISTS book(" +
      "id INT NOT NULL AUTO_INCREMENT," +
      "title VARCHAR(45) NOT NULL," +
      "description VARCHAR(255)," +
      "author VARCHAR(45) NOT NULL," +
      "language VARCHAR(45) NOT NULL," +
      "release_date DATE NOT NULL," +
      "publisher VARCHAR(45) NOT NULL," +
      "pages INT NOT NULL," +
      "PRIMARY KEY(id));";
    try  {
      Statement statement = this.connection.createStatement();
      statement.execute(create);
      System.out.println("Table book created");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to create table book " + exception.getMessage());
      throw new RuntimeException("Database Error", exception);
    }
  }

  @Override
  public void dropTable() {
    String drop = "DROP TABLE IF EXISTS book;";
    try {
      Statement statement = this.connection.createStatement();
      statement.executeUpdate(drop);
      System.out.println("Table book dropped");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to drop table book " + exception.getMessage());
      throw new RuntimeException("Database Error", exception);
    }
  }

  @Override
  public void truncateTable() {
    String truncate = "TRUNCATE TABLE book;";
    try {
      Statement statement = this.connection.createStatement();
      statement.executeUpdate(truncate);
      System.out.println("Table book truncated");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to truncate table book " + exception.getMessage());
      throw new RuntimeException("Database Error", exception);
    }
  }
}
