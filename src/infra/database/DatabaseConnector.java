package infra.database;

import io.github.cdimascio.dotenv.Dotenv;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnector {
  public static Connection getConnection() {
    Dotenv dotenv = Dotenv.load();
    String URL = dotenv.get("DB_URL");
    String USER = dotenv.get("DB_USER");
    String PASSWORD = dotenv.get("DB_PASS");
    try {
      Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
      System.out.println("Connection stablished to database");
      return connection;
    } catch (SQLException exception) {
      System.err.println("There was an error trying to connect to database " + exception.getMessage());
      throw new RuntimeException("Database Error: ", exception);
    }
  }
}
