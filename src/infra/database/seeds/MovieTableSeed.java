package infra.database.seeds;

import infra.database.seeds.interfaces.Seed;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class MovieTableSeed implements Seed {
  private final Connection connection;

  public MovieTableSeed(Connection connection) {
    this.connection = connection;
  }

  @Override
  public void bulkInsert() {
    String bulk = "INSERT INTO movie(title, director, release_date) VALUES " +
      "('Asteroid City', 'Wes Anderson', '2023-08-10')," +
      "('Midsommar', 'Ari Aster', '2019-09-19')," +
      "('Inglorious Basterds', 'Quentin Tarantino', '2009-10-9')," +
      "('A Clockwork Orange', 'Stanley Kubrick', '1972-04-26');";
    try {
      Statement statement = this.connection.createStatement();
      statement.executeUpdate(bulk);
      System.out.println("Insert items into the database");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to insert into database " + exception.getMessage());
      throw new RuntimeException("Insertion error", exception);
    }
  }
}
