package infra.database.seeds;

import infra.database.seeds.interfaces.Seed;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class BookTableSeed implements Seed {
  private final Connection connection;

  public BookTableSeed(Connection connection) {
    this.connection = connection;
  }
  @Override
  public void bulkInsert() {
    String bulk = "INSERT INTO book(title, description, author, language, release_date, publisher, pages) VALUES" +
      "('Clockwork Orange', 'book', 'Anthony Burgess', 'english', '1962-01-01', 'pub', 192)," +
      "('Lord of the Flies', 'book', 'William Golding', 'english', '1954-10-17', 'pub', 216)," +
      "('Animal Farm', 'book', 'George Orwell', 'english', '1945-08-17', 'pub', 140);";
    try {
      Statement statement = this.connection.createStatement();
      statement.executeUpdate(bulk);
      System.out.println("Insert items into table book");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to insert into table book " + exception.getMessage());
      throw new RuntimeException("Database Error", exception);
    }
  }
}
