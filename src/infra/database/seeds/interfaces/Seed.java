package infra.database.seeds.interfaces;

public interface Seed {
  void bulkInsert();
}
