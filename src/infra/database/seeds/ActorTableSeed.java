package infra.database.seeds;

import infra.database.seeds.interfaces.Seed;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class ActorTableSeed implements Seed {
  private final Connection connection;

  public ActorTableSeed(Connection connection) {
    this.connection = connection;
  }

  @Override
  public void bulkInsert() {
    String insert = "INSERT INTO actor(name, bio) VALUES" +
      "('Leonardo Di Caprio', 'actor')," +
      "('Keanu Reeves', 'actor')," +
      "('Tom Cruise', 'actor')," +
      "('Brad Pitt', 'actor')," +
      "('Johnny Depp', 'actor')," +
      "('Angelina Jolie', 'actress')," +
      "('Megan Fox', 'actress');";
    try {
      Statement statement = this.connection.createStatement();
      statement.execute(insert);
      System.out.println("Inserting items into actor table");
    } catch (SQLException exception) {
      System.err.println("There was an error trying to insert items in actor table");
      throw new RuntimeException("Database Error", exception);
    }
  }
}
