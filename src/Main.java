import infra.database.DatabaseConnector;
import infra.database.migrations.ActorTableMigration;
import infra.database.migrations.BookTableMigration;
import infra.database.migrations.MoviesTableMigration;
import infra.database.seeds.ActorTableSeed;
import infra.database.seeds.BookTableSeed;
import infra.database.seeds.MovieTableSeed;
import infra.database.seeds.interfaces.Seed;

import java.sql.Connection;

public class Main {
  public static void main(String[] args) {
    Connection connection = DatabaseConnector.getConnection();
    MoviesTableMigration moviesTable = new MoviesTableMigration(connection);
    BookTableMigration bookTable = new BookTableMigration(connection);
    ActorTableMigration actorTable = new ActorTableMigration(connection);
    Seed moviesSeed = new MovieTableSeed(connection);
    Seed bookSeed = new BookTableSeed(connection);
    Seed actorSeed = new ActorTableSeed(connection);
    moviesTable.dropTable();
    bookTable.dropTable();
    actorTable.dropTable();
    moviesTable.createTable();
    moviesSeed.bulkInsert();
    moviesTable.alterTableAddDescriptionColumn();
    bookTable.createTable();
    bookSeed.bulkInsert();
    actorTable.createTable();
    actorSeed.bulkInsert();
  }
}
